<?php
 
class Forecast extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('forecast_model');
        $this->load->helper('form');

        if (!(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true)) {
            redirect('/logout');
        }
    } 

    /*
     * Listing of forecast
     */
    function index()
    {
        $data['forecast'] = $this->forecast_model->get_all_forecast();
        
        $data['_view'] = 'forecast/index';

        $this->load->view('header');
        $this->load->view('forecast/search', $data);
        $this->load->view('footer');
    }

    /*
     * Adding a new forecast
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'user_id'     => $this->input->post('user_id'),
				'location'    => $this->input->post('location'),
                'temperature' => $this->input->post('temperature'),
                'pressure'    => $this->input->post('pressure'),
                'humidity'    => $this->input->post('humidity'),
                'max_temp'    => $this->input->post('max_temp'),
                'min_temp'    => $this->input->post('min_temp'),
                'full_info'   => $this->input->post('full_info'),
                'favourite'   => true,
            );
            $forecast_id = $this->forecast_model->add_forecast($params);

            echo $forecast_id;
        }
        else
        {     
            echo 0;
        }
    }

    function add_marker()
    {   
        $this->load->model('mark_model');
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
                'name'         => $this->input->post('location'),
                'address'      => $this->input->post('location'),
                'lat'          => $this->input->post('lat'),
                'lng'          => $this->input->post('lng'),
                'type'         => 'default',
                'user_id'      => $this->input->post('user_id'),
                'favourite_id' => $this->input->post('favourite_id')
            );
            $mark_id = $this->mark_model->add_mark($params);

            echo $mark_id;
        }
        else
        {     
            echo 0;
        }
    }

    function get_marker($id='')
    {  
        $this->load->model('mark_model');
        if ($id=='') {
            echo json_encode( $this->mark_model->get_all_mark());
        }else{
            echo json_encode( $this->mark_model->get_mark($id));
        } 
    }  

    /*
     * Editing a forecast
     */
    function edit($forecast_id)
    {   
        // check if the forecast exists before trying to edit it
        $data['forecast'] = $this->forecast_model->get_forecast($forecast_id);
        
        if(isset($data['forecast']['forecast_id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'fullname' => $this->input->post('fullname'),
					'last_update' => $this->input->post('last_update'),
                );

                $this->forecast_model->update_forecast($forecast_id,$params);            
                redirect('forecast/index');
            }
            else
            {
                $data['_view'] = 'forecast/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The forecast you are trying to edit does not exist.');
    } 

    function like($forecast_id)
    {   
        // check if the forecast exists before trying to edit it
        $data['forecast'] = $this->forecast_model->get_forecast($forecast_id);
        
        if(isset($data['forecast']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                    'favourite' => 1,
                );

                $this->forecast_model->update_forecast($forecast_id,$params);
                echo json_encode($this->forecast_model->get_forecast($forecast_id));          
            }
            else
                echo 'There no post.';
        }
        else
            echo 'The forecast you are trying to edit does not exist.';
    }    

    function unlike($forecast_id)
    {   
        // check if the forecast exists before trying to edit it
        $data['forecast'] = $this->forecast_model->get_forecast($forecast_id);
        echo '<pre>';
        // var_dump($data);die();
        if(isset($data['forecast']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                    'favourite' => 0,
                );

                $this->forecast_model->update_forecast($forecast_id,$params);
                echo json_encode($this->forecast_model->get_forecast($forecast_id));          
            }
            else
                echo 'There no post.';
        }
        else
            echo 'The forecast you are trying to edit does not exist.';
    }

    /*
     * Deleting forecast
     */
    function remove($forecast_id)
    {
        $forecast = $this->forecast_model->get_forecast($forecast_id);

        // check if the forecast exists before trying to delete it
        if(isset($forecast['forecast_id']))
        {
            $this->forecast_model->delete_forecast($forecast_id);
            redirect('forecast/index');
        }
        else
            show_error('The forecast you are trying to delete does not exist.');
    }

    function search_city(){
        if ($this->uri->segment(4)=='') {
            $mode = 'city';
        }else{
            $mode = $this->uri->segment(4);
        }
        $val = $this->uri->segment(3);
        switch ($mode) {
            case 'city':
                if ($val=='') {
                    $val = 'Buenos Aires';
                }
                break;
            case 'zip':
                if ($val=='') {
                    $val = '1405';
                }
                break;

            case 'coords':
                if ($val=='') {
                    $val = '-34.6036051:-58.3903248';
                }
                break;

            default:
                if ($val=='') {
                    $val = 'Buenos Aires';
                }
                break;
        }
        $data['mode'] = ['mode'=>$mode, 'val'=>$val];
        // var_dump($data);
        $this->load->view('forecast/search_curl', $data);
    }

}


