<div class="container well">
	<div class="col-md-5">
		<div class="page-header">
			<h3>Weather App</h3>
		</div>
		<form action="" method="post" name="search-weathe-form">
			<div class="col-md-12 well">
				<div class="form-group">
					<label for="search-weather">Search Weather</label>
					<input type="hidden" id="forecast_api_key" value="<?php echo MAP_KEY; ?>">
					<input type="text" class="form-control" id="search-weather" id="search-weather" name="search-weather" placeholder="cyty || zipcode || lat,lng">
				</div>
				<div class="alert alert-warning alert-dismissible" id="errors">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <span class="content"></span>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<button data-color="success" data-action="city" id="search-btn-success" class="btn-search btn-lg btn btn-success btn-block">City</button>
					</div>
					<div class="col-md-4">
						<button data-color="info" data-action="zip" id="search-btn-info" class="btn-search btn-lg btn btn-info btn-block">Zipcode</button>
					</div>
					<div class="col-md-4">
						<button data-color="warning" data-action="coords" id="search-btn-warning" class="btn-search btn-lg btn btn-warning btn-block">Lat/Lng</button>
					</div>
				</div>
			</div>
		</form>
		<br>
		<div class="well" id="detail-forecast">

			<ul class="list-group" >
				<?php 
				// if (isset($_SESSION['username'])){
				// 	printf('<li>
				// 				<b>Nick: </b>
				// 				<span class="nick">%s</span>
				// 			</li>', $_SESSION['username']);
				// }
				?>
				<li class="list-group-item">
					<b>Location: </b>
					<span class="location">Buenos Aires, AR</span>

				</li>
				<li class="list-group-item">
					<b>Temperature: </b>
					<span class="temperature">-</span>

				</li>
				<li class="list-group-item">
					<b>Presure: </b>
					<span class="pressure">-</span>

				</li>
				<li class="list-group-item">
					<b>Humidity: </b>
					<span class="humidity">-</span>

				</li>
				<li class="list-group-item">
					<b>Max. Temp.: </b>
					<span class="max_temp">-</span>

				</li>
				<li class="list-group-item">
					<b>MIn. Temp.: </b>
					<span class="min_temp">-</span>

				</li>
			</ul>
			<form action="<?php echo base_url('forecast/add') ?>" method="post" id="save-favourite-form">
				<input type="hidden" id="user_id"      name="user_id" value="<?php echo $this->session->userdata('user_id') ?>">
				<input type="hidden" id="location"     name="location">
				<input type="hidden" id="temperature"  name="temperature">
				<input type="hidden" id="pressure"     name="pressure">
				<input type="hidden" id="humidity"     name="humidity">
				<input type="hidden" id="max_temp"     name="max_temp">
				<input type="hidden" id="min_temp"     name="min_temp">
				<input type="hidden" id="lat"          name="lat">
				<input type="hidden" id="lon"          name="lon">
				<input type="hidden" id="full_info"    name="full_info">
				<input type="hidden" id="valid_form"   name="valid_form">
				<input type="hidden" id="favourite_id" name="favourite_id" value="0">

				<button id="save-favourite-btn" class="btn-lg btn btn-danger btn-block">Save Favourite</button>
				<button id="save-marker-btn" class="hidden" data-action="<?php echo base_url('forecast/add_marker') ?>">Save Favourite</button>
			</form>
		</div>
	</div>
	<div class="col-md-7">
		<div class="page-header">
			<h3 class="text-right">
				<div class="pull-right">
					<button class="btn btn-xs btn-primary list-toggle">Favourites</button>
					<button class="btn btn-xs btn-primary">Graphs</button>
				</div>
			</h3>
		</div>
		<div id="map"></div>
		<div class="row">
			<h2>Tips</h2>
			<ul class="list-group">
				<li class="list-group-item">Search City -> {city_name(string, n)}-{city_code(string, 2, optional) }</li>
				<li class="list-group-item">Zipcode     -> {zipcode(int, n)}-{city_code(string, 2, optional) }</li>
				<li class="list-group-item">Coords      -> {lat(float, n)}:{lon(float, n) }</li>
				<li class="list-group-item">Map         -> You can generate coords at dragend of the i icon in the center map</li>
			</ul>
		</div>
	</div>
	<fieldset class="col-md-12" id="favourite_list">
		<legend>Favourites List</legend>
		<table class="table table-hover table-striped" id="table-datatable"  data-order='[[ 1, "asc" ]]' data-page-length='25'>
			<thead>
				<tr>
					<th>Location</th>
					<th>Temp.(°C)</th>
					<th>Pressure (mmHg)</th>
					<th>Humidity (%)</th>
					<th>Max.Temp.(°C)</th>
					<th>Min.Temp.(°C)</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					for ($i=0; $i < count($forecast) ; $i++) {
						if ($forecast[$i]['favourite']!=1) {
							$button = '<a id="like-'.$forecast[$i]['id'].'" href="'.base_url('forecast/like/'.$forecast[$i]['id']).'" class="btn btn-xs btn-list btn-info like">like</a>';
						}else{
							$button = '<a id="like-'.$forecast[$i]['id'].'" href="'.base_url('forecast/unlike/'.$forecast[$i]['id']).'" class="btn btn-xs btn-list btn-success unlike">unlike</a>';
						}
						printf('
								<tr>
									<td>%s</td>
									<td>%s</td>
									<td>%s</td>
									<td>%s</td>
									<td>%s</td>
									<td>%s</td>
									<td>%s</td>
									<td>%s</td>
								</tr>
							',
							$forecast[$i]['location'],
							$forecast[$i]['temperature'],
							$forecast[$i]['pressure'],
							$forecast[$i]['humidity'],
							$forecast[$i]['max_temp'],
							$forecast[$i]['min_temp'],
							$forecast[$i]['id'], 
							$button
						);
					}
				?>
			</tbody>
		</table>
	</fieldset>
</div>
<script>

$(function(){

	// ocultar espacio de errores
	$('#errors').slideUp();
	// $('#save-favourite-btn').slideUp();
	$('#detail-forecast').slideUp();

	$('#favourite_list').slideUp();
	// TODO: isn´t work
	// $('#table-datatable').dataTable();

	// load markers
	var url_markers = '<?php echo base_url('forecast/get_marker') ?>';
	load_markers(url_markers);
	// triggers

	/*
		one trigger for serach
		serch by city, zipcode, coords
		city = city(string) or id_city(int, 5)
		zipcode = zipcode(int, 5)-city_code(string, 2)(optional='ar')
		coords = latitud(float, 6):longitud(float, 6);
	*/
	$('.btn-search').on('click', function(e){
		e.preventDefault();
		// search
		var input_value = $('#search-weather').val().trim();
		if (input_value !='') {
			var action    = $(this).attr('data-action');
			var btn_class = $(this).attr('data-color');

			if (action == 'city') {
				input_value = valid_input_city(input_value);
				uri4 = '';
			}
			if (action == 'zip') {
				input_value = valid_input_zip(input_value);
				uri4 = '/zip';
			}
			if (action == 'coords') {
				input_value = valid_input_coords(input_value);
				uri4 = '/coords';
			}


			var url = '<?php echo base_url('forecast/search_city/'); ?>' + input_value + uri4;
			console.log(url);
			weather_by_city_name(url, btn_class);
		}else{
			search_error($(this));
		}
	})

	$('#save-favourite-btn').on('click', function(e){
		e.preventDefault();
		if (save_favourite_form_valid()) {
			var url = $('#save-favourite-form').attr('action');
			save_favourite_form(url);
		}else{
			save_favourite_form_error();
		}
	})

	$('#save-marker-btn').on('click', function(e){
		e.preventDefault();
		if (save_marker_form_valid()) {
			save_marker();
		}else{
			save_marker_error();
		}
	})

	$('.btn-list').on('click', function(e){
		e.preventDefault();
		var url = $(this).attr('href'); 
		var id = $(this).attr('id');
		if($(this).hasClass('like')){
			$(this)
				.removeClass('like')
				.removeClass('btn-info')
				.addClass('btn-success unlike')
				.attr('href', '<?php echo base_url("forecast/unlike/") ?>' + id.replace('like-', ''));
		}else{
			$(this)
				.removeClass('unlike')
				.removeClass('btn-success')
				.addClass('btn-info like')
				.attr('href', '<?php echo base_url("forecast/like/") ?>' + id.replace('like-', ''));
		}
		$.ajax({
			type        : 'post',
			dataType    : 'json',
			url         : url,
			data        : {url:url, id:id},
		})
		.done(function(res){
			console.log(res);
		});
	})

	$('.list-toggle').on('click', function(e){
		e.preventDefault();
		var el = $('#favourite_list');
		if(el.hasClass('active')){
			el.slideUp();
		}else{
			el.slideDown();
		}
		el.toggleClass('active');
	})
})
</script>
