<div class="container">
	<div class="well">
		<pre><?php var_dump($this->session->userdata); ?></pre>
	</div>
	<div class="col-md-5">
		<?php echo form_open('forecast/search'); ?>
			<div class="form-group">
				<label for="place">Search Place</label>
				<input type="text" class="form-control" id="place" name="place" placeholder="Enter a place" value="<?php echo $this->input->post('place'); ?>">
				<p class="help-block">At least 4 characters</p>
			</div>		
			<button type="submit" class="btn btn-lg btn-success">Search</button>

		<?php echo form_close(); ?>
	</div>
	<div class="col-md-7">
		<!-- google map -->
			<div id="map"></div>
		<!-- /google map -->
	</div>
</div>
<hr>
<hr>
<div class="container">
	<?php echo form_open('forecast/add'); ?>
		<div class="form-group">
			<label for="place">Search Place</label>
			<input type="text" class="form-control" id="place" name="place" placeholder="Enter a place" value="<?php echo $this->input->post('place'); ?>">
			<p class="help-block">At least 4 characters</p>
		</div>		
		<button type="submit" class="btn btn-lg btn-success">Save Favourite</button>

	<?php echo form_close(); ?>
</div>

			<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false'></script>
<script>
	// function initMap(arr='') {
	// 	if (arr=='') {
	// 		console.log('error, me voy al obelisco');
	// 		var places = [{lat: -34.603323, lng: -58.381613, zoom:2}];
	// 	}else{
	// 		var places = arr;
	// 	}
	// 	console.log(places);
	// 	var map = new google.maps.Map(document.getElementById('map'), {
	// 		zoom: places[0].zoom,
	// 		center: {lat:places[0].lat, lng:places[0].lng}
	// 	});

	// 	var marker = new google.maps.Marker({
	// 		position: places,
	// 		map: map
	// 	});

	// 	for (var i = 0; i < places.length; i++) {
	// 		var marker = new google.maps.Marker({
	// 			position: {lat: places[i].lat, lng:places[i].lng},
	// 			map: map
	// 		});
	// 	}
	// }
	// $(function(){
	// 	// initMap([{lat:-25.363, lng:131.044, zoom:5}, {lat: -34.603323, lng: -58.381613, zoom:5}]);
	// })

	var marcadores = [];

$(function(){

	var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
	var icons = {
		parking: {
			icon: iconBase + 'parking_lot_maps.png'
		},
		library: {
			icon: iconBase + 'library_maps.png'
		},
		info: {
			icon: iconBase + 'info-i_maps.png'
		}
	};

	function mapaGoogle() {

		var localidades = [
			['León', 42.603, -5.577],
			['Salamanca', 40.963, -5.669],
			['Zamora', 41.503, -5.744]
		];

		var mapa = new google.maps.Map(document.getElementById('map'), {
			zoom: 7,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});

		var limites = new google.maps.LatLngBounds();

		var infowindow = new google.maps.InfoWindow();

		var marcador, i;

		for (i = 0; i < localidades.length; i++) {

		marcador = new google.maps.Marker({
			position: new google.maps.LatLng(localidades[i][1], localidades[i][2]),
			map: mapa, 
			// icon: 'library'
			});

			marcadores.push(marcador);

			limites.extend(marcador.position);

			google.maps.event.addListener(marcador, 'click', (function(marcador, i) {
			return function() {
				infowindow.setContent(localidades[i][0]);
				infowindow.open(mapa, marcador);
				alert('eso es otra vez ');
			}
			})(marcador, i));
		}
		mapa.fitBounds(limites);
	}

	google.maps.event.addDomListener(window, 'load', mapaGoogle);
})
</script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API ?>" async defer></script> -->
 <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Precio Mercado', 1000],
          ['Mercado Descontado', 950],
          ['Precio Venta', 880],
          ['Precio Compra', 750],
          ['Sale Price', 800], 
          ['Buy Price', 740]
        ]);

        // Set chart options
        var options = {'title':'Relacion de Precios',
                       'width':500,
                       'height':500
                     };

        // Instantiate and draw our chart, passing in some options.
        //AreaChart
        //PieChart
        //BarChart
        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);

        var barchart_options = {
                      title:'Barchart: How Much Pizza I Ate Last Night',
                      width:500,
                      height:500,
                      legend: 'none'
                    };
        var barchart = new google.visualization.BarChart(document.getElementById('chart_div_2'));
        barchart.draw(data, barchart_options);

      }
    </script>