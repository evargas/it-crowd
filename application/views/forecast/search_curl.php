<?php 

	// cross origin & api_key protect

		// cross origin
		header("Access-Control-Allow-Origin:*");
		// city/zip/coords
		$val  = $mode['val'];
		$mode = $mode['mode'];
		// {city_name}/{city name},{country code}/lat={lat}&lon={lon} 
		switch ($mode) {
			case 'city':
				$url_piece = 'q='.$val;
				break;
			case 'zip':
				$val = explode( '-', $val);
				if (count($val)==1) {
					$val[1] = 'ar';
				}
				$url_piece = 'zip='.$val[0].','.$val[1];
				break;
			case 'coords':
				$val = explode(':', $val);
				$url_piece = 'lat='.$val[0].'&lon='.$val[1];
				break;
			default:
				$url_piece = 'q='.$val;
				break;
		}

		// api_key from https://openweathermap.org/
		$forecast_key = '48873f96a34dafac067791b4bb5e8160';

		// weather api from https://openweathermap.org/
		$url = 'http://api.openweathermap.org/data/2.5/weather?';
		$url.= $url_piece.'&units=metric&appid='.$forecast_key;

		// var_dump($url);die();// debug url
		// curl to extern api
		$cliente   = curl_init($url);
		$respuesta = curl_exec($cliente);
		curl_close($cliente);
?>

