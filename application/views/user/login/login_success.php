<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="page-header">
				<h1>Login success!</h1>
			</div>
			<p>You are now logged in.</p>
		</div>
		<div class="col-md-6">
			<div class="page-header">
				<h1>Check the forecast webapp</h1>
			</div>
			<p><a href="<?php echo base_url('/forecast') ?>" class="btn btn-lg btn-success"> Forecast Application </a></p>
		</div>
	</div><!-- .row -->
</div><!-- .container -->