/*
	functions	
*/
function weather_by_city_name(url, btn_class='success'){
	var btn_text    = $('#search-btn-'+btn_class).text();
	var city_ajax   = $.ajax({
			type        : 'get',
			dataType    : 'json',
			url         : url,
		    crossDomain : true,
			data        : {
				url:url
			},
			beforeSend: function(){
				$('#search-btn-'+btn_class).addClass('disabled btn-default').removeClass('btn-'+btn_class).text('Searching...');
			}
	})
	.done(function(response) {
		console.log(response);
		if (response.cod == 404 || response.cod == 401) {
			reset_search();
		}else{
			if (response.cod != 400) {
				if (response.name != undefined && response.sys.country != undefined){
					$('#location').val(response.name+'/'+response.sys.country);
				}else{
					$('#location').val('Intenta acercando el mapa sobre una ciudad');
				}
				if (response.main != undefined) {
					$('#temperature').val(response.main.temp);
					$('#pressure').val(response.main.pressure);
					$('#humidity').val(response.main.humidity);
					$('#max_temp').val(response.main.temp_max);
					$('#min_temp').val(response.main.temp_min);
					$('#lat').val(response.coord.lat);
					$('#lon').val(response.coord.lon);
					$('#full_info').val(JSON.stringify(response));
					$('.location').text(response.name+'/'+response.sys.country);
					$('.temperature').text(response.main.temp + '(Celsius)');
					$('.pressure').text(response.main.pressure + '(mmHg)');
					$('.humidity').text(response.main.humidity + '(%)');
					$('.max_temp').text(response.main.temp_max + '(Celsius)');
					$('.min_temp').text(response.main.temp_min + '(Celsius)');
					$('#valid_form').val('rodrigo');
					remove_error();
				}
			}else{
				$('#location').val(response.message);
			}
		}

		$('#search-btn-'+btn_class).addClass('btn-'+btn_class).removeClass('disabled btn-default').text(btn_text);	
		$('#save-favourite-btn').removeClass('disabled btn-default').addClass('btn-primary').slideDown().text('Save Favourite');
		$('#detail-forecast').slideDown();
	});
}


// TODO: faltan validaciones para los campos
function valid_input_city(input){
	input = input;
	return input;
}

function valid_input_zip(input){
	input = input;
	return input;
}

function valid_input_coords(input){
	input = input;
	return input;
}

function search_error(){
	$('#search-weather').attr('placeholder', 'Enter a valid zipcode {zipcode, (int,4-5)}-{code_country(string, 2)}').addClass('error');
	$('#errors').find('.content').text('Write a {city} / {zipcode}-{city code} or {lat}-{lon}');
	$('#errors').slideDown();
}

function remove_error(){
	$('#search-weather').removeClass('error').addClass('success');
	$('#errors').text('');
	$('#errors').slideUp();
}

function reset_search(){
	$('#search-weather').val('');
	$('#location').val('');
	$('#temperature').val('');
	$('#pressure').val('');
	$('#humidity').val('');
	$('#max_temp').val('');
	$('#min_temp').val('');
	$('#valid_form').val('');
	$('#full_info').text('');
	$('.location').text('No Found');
	$('.temperature').text('');
	$('.pressure').text('');
	$('.humidity').text('');
	$('.max_temp').text('');
	$('.min_temp').text('');
};
	

function save_favourite_form_valid(){
	var valid = true;
	if ($('#valid_form').val()!='rodrigo'){
		valid = false;
	}
	return valid;
}

function save_favourite_form_error(){
	$('#save-favourite-btn').text('Error al Guardar').addClass('btn-danger');
}

function save_favourite_form(url){
	var lat = $('#lat').val();
	var lng = $('#lon').val();
	var post = {
		user_id     : $('#user_id').val(),
		location    : $('#location').val(),
		temperature : $('#temperature').val(),
		pressure    : $('#pressure').val(),
		humidity    : $('#humidity').val(),
		max_temp    : $('#max_temp').val(),
		min_temp    : $('#min_temp').val(),
		full_info   : $('#full_info').val()
	};
	$.ajax({
		type        : 'post',
		dataType    : 'json',
		url         : url,
		data        : post,
		beforeSend: function(){
			console.log('saving save_favourite', post);
			$('#save-favourite-btn').addClass('disabled btn-default').removeClass('btn-primary').text('Saving ...');
			if (!save_favourite_form_valid()) {
				alert('something happen beforeSend');
				return false;
			}
		},
		error: function(){
			console.log('something happen save_favourite_form error');
			$('#save-favourite-btn').removeClass('btn-default').addClass('btn-primary').text('Error!');
		}
	})
	.done(function(res){
		$('#save-favourite-btn').removeClass('btn-default').addClass('btn-primary').text('Saved!');
		if (res!=0) {
			console.log('favorito guardado', res);
			$('#favourite_id').val(res);
			$('#save-marker-btn').trigger('click');
		}else{
			console.log('no pudo guardar el favorito', res);
		}
	});

}

function save_marker(marker_obj){
	url  = $('#save-marker-btn').attr('data-action');
	post = {
		name         : $('#location').val(),
		location     : $('#location').val(),
		lat          : $('#lat').val(),
		lng          : $('#lon').val(),
		user_id      : $('#user_id').val(),
		favourite_id : $('#favourite_id').val(),
	};
	$.ajax({
		type        : 'post',
		dataType    : 'json',
		url         : url,
		data        : post,
		beforeSend: function(){
			console.log('saving save_marker', post);
		},
		error: function(){
			console.log('saving save_marker', favourite_id);
			$('#save-favourite-btn').removeClass('btn-default').addClass('btn-primary').text('Error Saving Marker!');
		}
	})
	.done(function(res){
		console.log('Marker Saved!!!'+ favourite_id, res);
	});
}

function save_marker_form_valid(){
	var valid = true;
	var favourite = parseInt($('#favourite_id').val());
	console.log(favourite);
	if (!isNaN(favourite) && favourite == 0) {
		valid = false;
	}
	return valid;
}

function save_marker_error(){
	console.log('No se puede guardar el marker');
}