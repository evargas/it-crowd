-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para ci_crud
CREATE DATABASE IF NOT EXISTS `ci_crud` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ci_crud`;

-- Volcando estructura para tabla ci_crud.ci_sessions
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ci_crud.ci_sessions: ~8 rows (aproximadamente)
DELETE FROM `ci_sessions`;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
	('0qtpivbju5tf25rrp0skkkgu35', '127.0.0.1', 1521237850, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373835303B),
	('4138tgm1es28rrano72i4cbpvu', '127.0.0.1', 1521237373, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373337333B),
	('83jqdc257lr6knp8im97dhgjac', '127.0.0.1', 1521237797, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373739373B),
	('au4akjekamdqapuhv5ic66qj9m', '127.0.0.1', 1521237377, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373337373B),
	('bvold1ucrg1egf3adqqis9dj1g', '127.0.0.1', 1521237416, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373431363B686F6C617C733A323A227475223B),
	('k11rno9282kdpo0tv5s0lggdtj', '127.0.0.1', 1521420223, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313432303232333B),
	('p5qga2uvte0mi0qtbm99sqr7pg', '127.0.0.1', 1521237413, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373431333B757365725F69647C693A323B757365726E616D657C733A373A22726F647269676F223B6C6F676765645F696E7C623A313B69735F636F6E6669726D65647C623A303B69735F61646D696E7C623A303B),
	('tabt3m5lns01fgt8o4okiq31gk', '127.0.0.1', 1521241339, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313234313333373B),
	('uvfmaap4s27cuhsle0t6tfkpa2', '127.0.0.1', 1521237822, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373832323B);
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;

-- Volcando estructura para tabla ci_crud.favourite
CREATE TABLE IF NOT EXISTS `favourite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `location` varchar(100) NOT NULL,
  `temperature` float NOT NULL,
  `pressure` float NOT NULL,
  `humidity` float NOT NULL,
  `max_temp` float NOT NULL,
  `min_temp` float NOT NULL,
  `full_info` json NOT NULL,
  `id_marker` int(11) NOT NULL,
  `favourite` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_favourite_users` (`user_id`),
  KEY `id_marker` (`id_marker`),
  CONSTRAINT `FK_favourite_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 COMMENT='Lugares favoritos por los usuarios';

-- Volcando datos para la tabla ci_crud.favourite: ~26 rows (aproximadamente)
DELETE FROM `favourite`;
/*!40000 ALTER TABLE `favourite` DISABLE KEYS */;
INSERT INTO `favourite` (`id`, `user_id`, `location`, `temperature`, `pressure`, `humidity`, `max_temp`, `min_temp`, `full_info`, `id_marker`, `favourite`) VALUES
	(1, 1, 'Caracas/VE', 28, 1010, 65, 28, 28, '{"dt": 1521406800, "id": 3646738, "cod": 200, "sys": {"id": 4313, "type": 1, "sunset": 1521412706, "country": "VE", "message": 0.0023, "sunrise": 1521369145}, "base": "stations", "main": {"temp": 28, "humidity": 65, "pressure": 1010, "temp_max": 28, "temp_min": 28}, "name": "Caracas", "wind": {"deg": 70, "speed": 1.5}, "coord": {"lat": 10.51, "lon": -66.91}, "clouds": {"all": 20}, "weather": [{"id": 801, "icon": "02d", "main": "Clouds", "description": "few clouds"}], "visibility": 10000}', 0, 1),
	(2, 1, 'Sao Paulo/BR', 25.15, 1013, 83, 27, 23, '{"dt": 1521410400, "id": 3448439, "cod": 200, "sys": {"id": 4575, "type": 1, "sunset": 1521407906, "country": "BR", "message": 0.0128, "sunrise": 1521364187}, "base": "stations", "main": {"temp": 25.15, "humidity": 83, "pressure": 1013, "temp_max": 27, "temp_min": 23}, "name": "Sao Paulo", "wind": {"deg": 170, "speed": 3.1}, "coord": {"lat": -23.55, "lon": -46.64}, "clouds": {"all": 40}, "weather": [{"id": 802, "icon": "03n", "main": "Clouds", "description": "scattered clouds"}], "visibility": 10000}', 0, 1),
	(3, 1, 'Santiago/CL', 24, 1013, 31, 24, 24, '{"dt": 1521410400, "id": 3871336, "cod": 200, "sys": {"id": 4645, "type": 1, "sunset": 1521413703, "country": "CL", "message": 0.0171, "sunrise": 1521369902}, "base": "stations", "main": {"temp": 24, "humidity": 31, "pressure": 1013, "temp_max": 24, "temp_min": 24}, "name": "Santiago", "wind": {"deg": 180, "speed": 5.1}, "coord": {"lat": -33.44, "lon": -70.65}, "clouds": {"all": 0}, "weather": [{"id": 800, "icon": "01d", "main": "Clear", "description": "clear sky"}], "visibility": 10000}', 0, 1),
	(4, 1, 'Quito/EC', 15.11, 785.82, 100, 15.11, 15.11, '{"dt": 1521411683, "id": 3652462, "cod": 200, "sys": {"sunset": 1521415500, "country": "EC", "message": 0.0022, "sunrise": 1521371908}, "base": "stations", "main": {"temp": 15.11, "humidity": 100, "pressure": 785.82, "temp_max": 15.11, "temp_min": 15.11, "sea_level": 1019.57, "grnd_level": 785.82}, "name": "Quito", "rain": {"3h": 4.155}, "wind": {"deg": 288.001, "speed": 1.2}, "coord": {"lat": -0.22, "lon": -78.51}, "clouds": {"all": 92}, "weather": [{"id": 501, "icon": "10d", "main": "Rain", "description": "moderate rain"}]}', 0, 1),
	(5, 1, 'Rome/IT', 11.68, 999, 76, 13, 10, '{"dt": 1521410100, "id": 6539761, "cod": 200, "sys": {"id": 5848, "type": 1, "sunset": 1521393649, "country": "IT", "message": 0.0068, "sunrise": 1521350136}, "base": "stations", "main": {"temp": 11.68, "humidity": 76, "pressure": 999, "temp_max": 13, "temp_min": 10}, "name": "Rome", "wind": {"deg": 200, "speed": 3.1}, "coord": {"lat": 41.89, "lon": 12.48}, "clouds": {"all": 40}, "weather": [{"id": 802, "icon": "03n", "main": "Clouds", "description": "scattered clouds"}], "visibility": 10000}', 0, 1),
	(6, 1, 'Sidney/US', -0.29, 1016, 86, 1, -1, '{"dt": 1521410160, "id": 5677735, "cod": 200, "sys": {"id": 1749, "type": 1, "sunset": 1521421679, "country": "US", "message": 0.002, "sunrise": 1521378091}, "base": "stations", "main": {"temp": -0.29, "humidity": 86, "pressure": 1016, "temp_max": 1, "temp_min": -1}, "name": "Sidney", "wind": {"deg": 60, "speed": 4.1}, "coord": {"lat": 47.72, "lon": -104.16}, "clouds": {"all": 90}, "weather": [{"id": 721, "icon": "50d", "main": "Haze", "description": "haze"}, {"id": 600, "icon": "13d", "main": "Snow", "description": "light snow"}], "visibility": 9656}', 0, 1),
	(7, 1, 'Munich/DE', -4.01, 1006, 73, -3, -5, '{"dt": 1521409800, "id": 2867714, "cod": 200, "sys": {"id": 4887, "type": 1, "sunset": 1521393874, "country": "DE", "message": 0.0366, "sunrise": 1521350353}, "base": "stations", "main": {"temp": -4.01, "humidity": 73, "pressure": 1006, "temp_max": -3, "temp_min": -5}, "name": "Munich", "wind": {"deg": 80, "speed": 6.2}, "coord": {"lat": 48.14, "lon": 11.58}, "clouds": {"all": 90}, "weather": [{"id": 600, "icon": "13n", "main": "Snow", "description": "light snow"}], "visibility": 10000}', 0, 1),
	(8, 1, 'Paris/FR', -1, 1008, 92, -1, -1, '{"dt": 1521410400, "id": 2988507, "cod": 200, "sys": {"id": 5610, "type": 1, "sunset": 1521396094, "country": "FR", "message": 0.0069, "sunrise": 1521352564}, "base": "stations", "main": {"temp": -1, "humidity": 92, "pressure": 1008, "temp_max": -1, "temp_min": -1}, "name": "Paris", "wind": {"deg": 70, "speed": 2.6}, "coord": {"lat": 48.86, "lon": 2.35}, "clouds": {"all": 75}, "weather": [{"id": 600, "icon": "13n", "main": "Snow", "description": "light snow"}, {"id": 701, "icon": "50n", "main": "Mist", "description": "mist"}], "visibility": 4600}', 0, 1),
	(9, 1, 'Lima/PE', 18.67, 875.9, 70, 18.67, 18.67, '{"dt": 1521413169, "id": 3936456, "cod": 200, "sys": {"sunset": 1521415167, "country": "PE", "message": 0.0019, "sunrise": 1521371526}, "base": "stations", "main": {"temp": 18.67, "humidity": 70, "pressure": 875.9, "temp_max": 18.67, "temp_min": 18.67, "sea_level": 1022.37, "grnd_level": 875.9}, "name": "Lima", "wind": {"deg": 195.003, "speed": 1.03}, "coord": {"lat": -12.06, "lon": -77.04}, "clouds": {"all": 8}, "weather": [{"id": 800, "icon": "02d", "main": "Clear", "description": "clear sky"}]}', 0, 1),
	(10, 3, 'Mexico/US', -6.3, 1005, 43, -4, -11, '{"dt": 1521412500, "id": 4971871, "cod": 200, "sys": {"id": 1374, "type": 1, "sunset": 1521413598, "country": "US", "message": 0.0044, "sunrise": 1521370037}, "base": "stations", "main": {"temp": -6.3, "humidity": 43, "pressure": 1005, "temp_max": -4, "temp_min": -11}, "name": "Mexico", "wind": {"deg": 320, "gust": 12.3, "speed": 7.2}, "coord": {"lat": 44.56, "lon": -70.55}, "clouds": {"all": 1}, "weather": [{"id": 800, "icon": "01d", "main": "Clear", "description": "clear sky"}], "visibility": 11265}', 0, 0),
	(11, 1, 'Quito/EC', 13.52, 786.17, 100, 13.52, 13.52, '{"dt": 1521413636, "id": 3652462, "cod": 200, "sys": {"sunset": 1521415500, "country": "EC", "message": 0.0036, "sunrise": 1521371908}, "base": "stations", "main": {"temp": 13.52, "humidity": 100, "pressure": 786.17, "temp_max": 13.52, "temp_min": 13.52, "sea_level": 1020.75, "grnd_level": 786.17}, "name": "Quito", "rain": {"3h": 2.5825}, "wind": {"deg": 158.003, "speed": 0.28}, "coord": {"lat": -0.22, "lon": -78.51}, "clouds": {"all": 92}, "weather": [{"id": 500, "icon": "10d", "main": "Rain", "description": "light rain"}]}', 0, 1),
	(12, 3, 'Lima/PE', 18.67, 875.9, 70, 18.67, 18.67, '{"dt": 1521413819, "id": 3936456, "cod": 200, "sys": {"sunset": 1521415166, "country": "PE", "message": 0.0073, "sunrise": 1521371526}, "base": "stations", "main": {"temp": 18.67, "humidity": 70, "pressure": 875.9, "temp_max": 18.67, "temp_min": 18.67, "sea_level": 1022.37, "grnd_level": 875.9}, "name": "Lima", "wind": {"deg": 195.003, "speed": 1.03}, "coord": {"lat": -12.06, "lon": -77.04}, "clouds": {"all": 8}, "weather": [{"id": 800, "icon": "02d", "main": "Clear", "description": "clear sky"}]}', 0, 1),
	(13, 1, 'Asuncion/PY', 32, 1004, 62, 32, 32, '{"dt": 1521410400, "id": 3439389, "cod": 200, "sys": {"id": 4608, "type": 1, "sunset": 1521410546, "country": "PY", "message": 0.0016, "sunrise": 1521366818}, "base": "stations", "main": {"temp": 32, "humidity": 62, "pressure": 1004, "temp_max": 32, "temp_min": 32}, "name": "Asuncion", "wind": {"deg": 20, "speed": 2.1}, "coord": {"lat": -25.3, "lon": -57.63}, "clouds": {"all": 20}, "weather": [{"id": 801, "icon": "02d", "main": "Clouds", "description": "few clouds"}], "visibility": 10000}', 0, 0),
	(14, 4, 'Montevideo/US', 4.3, 1015, 80, 5, 4, '{"dt": 1521412560, "id": 5038018, "cod": 200, "sys": {"id": 1543, "type": 1, "sunset": 1521419646, "country": "US", "message": 0.0028, "sunrise": 1521376069}, "base": "stations", "main": {"temp": 4.3, "humidity": 80, "pressure": 1015, "temp_max": 5, "temp_min": 4}, "name": "Montevideo", "wind": {"deg": 29.5028, "speed": 2.28}, "coord": {"lat": 44.94, "lon": -95.72}, "clouds": {"all": 90}, "weather": [{"id": 804, "icon": "04d", "main": "Clouds", "description": "overcast clouds"}], "visibility": 16093}', 0, 1),
	(15, 1, 'Rome/IT', 11.74, 999, 76, 13, 10, '{"dt": 1521411600, "id": 6539761, "cod": 200, "sys": {"id": 5848, "type": 1, "sunset": 1521393650, "country": "IT", "message": 0.0025, "sunrise": 1521350134}, "base": "stations", "main": {"temp": 11.74, "humidity": 76, "pressure": 999, "temp_max": 13, "temp_min": 10}, "name": "Rome", "wind": {"deg": 200, "speed": 2.6}, "coord": {"lat": 41.89, "lon": 12.48}, "clouds": {"all": 40}, "weather": [{"id": 802, "icon": "03n", "main": "Clouds", "description": "scattered clouds"}], "visibility": 10000}', 0, 1),
	(16, 1, 'Moscu/RO', -4.59, 1008.43, 85, -4.59, -4.59, '{"dt": 1521414479, "id": 683352, "cod": 200, "sys": {"sunset": 1521389945, "country": "RO", "message": 0.0027, "sunrise": 1521346431}, "base": "stations", "main": {"temp": -4.59, "humidity": 85, "pressure": 1008.43, "temp_max": -4.59, "temp_min": -4.59, "sea_level": 1024.03, "grnd_level": 1008.43}, "name": "Moscu", "wind": {"deg": 358.503, "speed": 5.58}, "coord": {"lat": 45.9, "lon": 27.93}, "clouds": {"all": 80}, "weather": [{"id": 803, "icon": "04n", "main": "Clouds", "description": "broken clouds"}]}', 0, 1),
	(17, 1, 'Bruselas/CO', 15.62, 808.46, 95, 15.62, 15.62, '{"dt": 1521415768, "id": 3672110, "cod": 200, "sys": {"sunset": 1521414935, "country": "CO", "message": 0.005, "sunrise": 1521371350}, "base": "stations", "main": {"temp": 15.62, "humidity": 95, "pressure": 808.46, "temp_max": 15.62, "temp_min": 15.62, "sea_level": 1019.05, "grnd_level": 808.46}, "name": "Bruselas", "wind": {"deg": 85.5028, "speed": 0.43}, "coord": {"lat": 1.78, "lon": -76.17}, "clouds": {"all": 24}, "weather": [{"id": 801, "icon": "02n", "main": "Clouds", "description": "few clouds"}]}', 0, 1),
	(18, 1, 'Tokio/US', 8, 1017, 70, 8, 8, '{"dt": 1521413880, "id": 5808453, "cod": 200, "sys": {"id": 2932, "type": 1, "sunset": 1521425071, "country": "US", "message": 0.0028, "sunrise": 1521381468}, "base": "stations", "main": {"temp": 8, "humidity": 70, "pressure": 1017, "temp_max": 8, "temp_min": 8}, "name": "Tokio", "wind": {"deg": 240, "speed": 6.2}, "coord": {"lat": 47.21, "lon": -118.27}, "clouds": {"all": 90}, "weather": [{"id": 804, "icon": "04d", "main": "Clouds", "description": "overcast clouds"}], "visibility": 16093}', 0, 0),
	(19, 5, 'Changai/GM', 25.22, 1019.06, 30, 25.22, 25.22, '{"dt": 1521416108, "id": 2413866, "cod": 200, "sys": {"sunset": 1521400166, "country": "GM", "message": 0.0389, "sunrise": 1521356616}, "base": "stations", "main": {"temp": 25.22, "humidity": 30, "pressure": 1019.06, "temp_max": 25.22, "temp_min": 25.22, "sea_level": 1022.69, "grnd_level": 1019.06}, "name": "Changai", "wind": {"deg": 6.50284, "speed": 4.23}, "coord": {"lat": 13.59, "lon": -14.67}, "clouds": {"all": 0}, "weather": [{"id": 800, "icon": "01n", "main": "Clear", "description": "clear sky"}]}', 0, 1),
	(20, 1, 'Habana/EC', 29, 1006, 70, 29, 29, '{"dt": 1521414000, "id": 3650186, "cod": 200, "sys": {"id": 4356, "type": 1, "sunset": 1521415798, "country": "EC", "message": 0.0024, "sunrise": 1521372202}, "base": "stations", "main": {"temp": 29, "humidity": 70, "pressure": 1006, "temp_max": 29, "temp_min": 29}, "name": "Habana", "wind": {"deg": 40, "speed": 1.5}, "coord": {"lat": -1.62, "lon": -79.75}, "clouds": {"all": 40}, "weather": [{"id": 802, "icon": "03d", "main": "Clouds", "description": "scattered clouds"}], "visibility": 10000}', 0, 1),
	(21, 1, 'Oregon/US', 11, 1014, 40, 11, 11, '{"dt": 1521413760, "id": 4904898, "cod": 200, "sys": {"id": 1006, "type": 1, "sunset": 1521418106, "country": "US", "message": 0.0034, "sunrise": 1521374536}, "base": "stations", "main": {"temp": 11, "humidity": 40, "pressure": 1014, "temp_max": 11, "temp_min": 11}, "name": "Oregon", "wind": {"deg": 170, "speed": 1.5}, "coord": {"lat": 42.01, "lon": -89.33}, "clouds": {"all": 1}, "weather": [{"id": 800, "icon": "01d", "main": "Clear", "description": "clear sky"}], "visibility": 16093}', 0, 1),
	(22, 4, 'Atlanta/US', 21.18, 1011, 28, 23, 19, '{"dt": 1521413760, "id": 4180439, "cod": 200, "sys": {"id": 748, "type": 1, "sunset": 1521416906, "country": "US", "message": 0.0033, "sunrise": 1521373354}, "base": "stations", "main": {"temp": 21.18, "humidity": 28, "pressure": 1011, "temp_max": 23, "temp_min": 19}, "name": "Atlanta", "wind": {"deg": 280, "speed": 1.5}, "coord": {"lat": 33.75, "lon": -84.39}, "clouds": {"all": 20}, "weather": [{"id": 801, "icon": "02d", "main": "Clouds", "description": "few clouds"}], "visibility": 16093}', 0, 1),
	(23, 1, 'Maracaibo/CO', 27, 1011, 61, 27, 27, '{"dt": 1521414000, "id": 3670764, "cod": 200, "sys": {"id": 4251, "type": 1, "sunset": 1521414840, "country": "CO", "message": 0.0028, "sunrise": 1521371259}, "base": "stations", "main": {"temp": 27, "humidity": 61, "pressure": 1011, "temp_max": 27, "temp_min": 27}, "name": "Maracaibo", "wind": {"deg": 320, "speed": 5.7}, "coord": {"lat": 3.41, "lon": -75.78}, "clouds": {"all": 75}, "weather": [{"id": 521, "icon": "09d", "main": "Rain", "description": "shower rain"}], "visibility": 9000}', 0, 0),
	(24, 1, 'San Antonio/CL', 13.37, 998.3, 80, 13.37, 13.37, '{"dt": 1521416809, "id": 3872395, "cod": 200, "sys": {"sunset": 1521413929, "country": "CL", "message": 0.0039, "sunrise": 1521370134}, "base": "stations", "main": {"temp": 13.37, "humidity": 80, "pressure": 998.3, "temp_max": 13.37, "temp_min": 13.37, "sea_level": 1027.23, "grnd_level": 998.3}, "name": "San Antonio", "wind": {"deg": 236.003, "speed": 1.63}, "coord": {"lat": -33.58, "lon": -71.61}, "clouds": {"all": 0}, "weather": [{"id": 800, "icon": "01n", "main": "Clear", "description": "clear sky"}]}', 0, 1),
	(25, 1, 'Merida/PH', 24.27, 1007.54, 100, 24.27, 24.27, '{"dt": 1521417073, "id": 1699827, "cod": 200, "sys": {"sunset": 1521366757, "country": "PH", "message": 0.0033, "sunrise": 1521323214}, "base": "stations", "main": {"temp": 24.27, "humidity": 100, "pressure": 1007.54, "temp_max": 24.27, "temp_min": 24.27, "sea_level": 1025.13, "grnd_level": 1007.54}, "name": "Merida", "rain": {"3h": 0.3325}, "wind": {"deg": 73.5028, "speed": 1.58}, "coord": {"lat": 10.91, "lon": 124.54}, "clouds": {"all": 68}, "weather": [{"id": 500, "icon": "10n", "main": "Rain", "description": "light rain"}]}', 0, 0),
	(26, 1, 'Trujillo/PE', 21, 1008, 88, 21, 21, '{"dt": 1521414000, "id": 3691175, "cod": 200, "sys": {"id": 4404, "type": 1, "sunset": 1521415635, "country": "PE", "message": 0.0286, "sunrise": 1521372014}, "base": "stations", "main": {"temp": 21, "humidity": 88, "pressure": 1008, "temp_max": 21, "temp_min": 21}, "name": "Trujillo", "wind": {"deg": 150, "speed": 3.6}, "coord": {"lat": -8.11, "lon": -79.03}, "clouds": {"all": 75}, "weather": [{"id": 803, "icon": "04d", "main": "Clouds", "description": "broken clouds"}], "visibility": 6000}', 0, 0),
	(27, 2, 'Palermo/CO', 32, 1005, 49, 32, 32, '{"dt": 1521414000, "id": 3673286, "cod": 200, "sys": {"id": 4265, "type": 1, "sunset": 1521501156, "country": "CO", "message": 0.0556, "sunrise": 1521457574}, "base": "stations", "main": {"temp": 32, "humidity": 49, "pressure": 1005, "temp_max": 32, "temp_min": 32}, "name": "Palermo", "wind": {"deg": 10, "speed": 3.6}, "coord": {"lat": 2.89, "lon": -75.43}, "clouds": {"all": 40}, "weather": [{"id": 802, "icon": "03d", "main": "Clouds", "description": "scattered clouds"}], "visibility": 8000}', 0, 0),
	(28, 1, 'Colonia/RO', -0.34, 1007, 92, 1, -1, '{"dt": 1521415800, "id": 664460, "cod": 200, "sys": {"id": 5988, "type": 1, "sunset": 1521477301, "country": "RO", "message": 0.0025, "sunrise": 1521433776}, "base": "stations", "main": {"temp": -0.34, "humidity": 92, "pressure": 1007, "temp_max": 1, "temp_min": -1}, "name": "Colonia", "wind": {"deg": 60, "speed": 3.1}, "coord": {"lat": 46.6, "lon": 23.97}, "clouds": {"all": 90}, "weather": [{"id": 701, "icon": "50n", "main": "Mist", "description": "mist"}], "visibility": 5000}', 0, 0),
	(29, 1, 'Rome/IT', 11.01, 999, 76, 12, 9, '{"dt": 1521417300, "id": 6539761, "cod": 200, "sys": {"id": 5848, "type": 1, "sunset": 1521480054, "country": "IT", "message": 0.0529, "sunrise": 1521436529}, "base": "stations", "main": {"temp": 11.01, "humidity": 76, "pressure": 999, "temp_max": 12, "temp_min": 9}, "name": "Rome", "wind": {"deg": 180, "speed": 2.1}, "coord": {"lat": 41.89, "lon": 12.48}, "clouds": {"all": 40}, "weather": [{"id": 802, "icon": "03n", "main": "Clouds", "description": "scattered clouds"}], "visibility": 10000}', 0, 0),
	(30, 4, 'California/MX', 24, 1018, 8, 24, 24, '{"dt": 1521417000, "id": 4011743, "cod": 200, "sys": {"id": 3983, "type": 1, "sunset": 1521508154, "country": "MX", "message": 0.0032, "sunrise": 1521464598}, "base": "stations", "main": {"temp": 24, "humidity": 8, "pressure": 1018, "temp_max": 24, "temp_min": 24}, "name": "California", "wind": {"deg": 240, "speed": 8.2}, "coord": {"lat": 24.05, "lon": -104.61}, "clouds": {"all": 75}, "weather": [{"id": 803, "icon": "04d", "main": "Clouds", "description": "broken clouds"}], "visibility": 19312}', 0, 1),
	(31, 1, 'Turin/IT', 3.56, 997, 100, 5, 2, '{"dt": 1521417300, "id": 3165524, "cod": 200, "sys": {"id": 5799, "type": 1, "sunset": 1521481212, "country": "IT", "message": 0.0037, "sunrise": 1521437678}, "base": "stations", "main": {"temp": 3.56, "humidity": 100, "pressure": 997, "temp_max": 5, "temp_min": 2}, "name": "Turin", "wind": {"deg": 260, "speed": 1.5}, "coord": {"lat": 45.07, "lon": 7.68}, "clouds": {"all": 75}, "weather": [{"id": 803, "icon": "04n", "main": "Clouds", "description": "broken clouds"}], "visibility": 10000}', 0, 1),
	(32, 1, 'Naples/IT', 10.5, 1000, 81, 12, 9, '{"dt": 1521417300, "id": 3172394, "cod": 200, "sys": {"id": 5859, "type": 1, "sunset": 1521479628, "country": "IT", "message": 0.0156, "sunrise": 1521436103}, "base": "stations", "main": {"temp": 10.5, "humidity": 81, "pressure": 1000, "temp_max": 12, "temp_min": 9}, "name": "Naples", "wind": {"speed": 1}, "coord": {"lat": 40.84, "lon": 14.25}, "clouds": {"all": 40}, "weather": [{"id": 802, "icon": "03n", "main": "Clouds", "description": "scattered clouds"}], "visibility": 10000}', 0, 1),
	(33, 1, 'Ross River/CA', -4.88, 892.41, 100, -4.88, -4.88, '{"dt": 1521425962, "id": 6127994, "cod": 200, "sys": {"sunset": 1521514536, "country": "CA", "message": 0.0019, "sunrise": 1521470750}, "base": "stations", "main": {"temp": -4.88, "humidity": 100, "pressure": 892.41, "temp_max": -4.88, "temp_min": -4.88, "sea_level": 1029.41, "grnd_level": 892.41}, "name": "Ross River", "wind": {"deg": 40.0023, "speed": 0.17}, "coord": {"lat": 61.85, "lon": -130.59}, "clouds": {"all": 24}, "weather": [{"id": 801, "icon": "02n", "main": "Clouds", "description": "few clouds"}]}', 0, 1),
	(34, 1, 'Dudinka/RU', -19.53, 1015.13, 75, -19.53, -19.53, '{"dt": 1521426001, "id": 1507116, "cod": 200, "sys": {"sunset": 1521462867, "country": "RU", "message": 0.0019, "sunrise": 1521419201}, "base": "stations", "main": {"temp": -19.53, "humidity": 75, "pressure": 1015.13, "temp_max": -19.53, "temp_min": -19.53, "sea_level": 1019.2, "grnd_level": 1015.13}, "name": "Dudinka", "wind": {"deg": 192.002, "speed": 6.57}, "coord": {"lat": 69.34, "lon": 84.57}, "clouds": {"all": 48}, "weather": [{"id": 802, "icon": "03d", "main": "Clouds", "description": "scattered clouds"}]}', 0, 1),
	(35, 1, 'Intenta acercando el mapa sobre una ciudad', -16.62, 722.8, 76, -16.62, -16.62, '{"dt": 1521466326, "id": 0, "cod": 200, "sys": {"sunset": 1521494162, "message": 0.0043, "sunrise": 1521449704}, "base": "stations", "main": {"temp": -16.62, "humidity": 76, "pressure": 722.8, "temp_max": -16.62, "temp_min": -16.62, "sea_level": 1031.12, "grnd_level": 722.8}, "name": "", "wind": {"deg": 234.002, "speed": 12.57}, "coord": {"lat": 75.8, "lon": -44.1}, "clouds": {"all": 56}, "weather": [{"id": 803, "icon": "04d", "main": "Clouds", "description": "broken clouds"}]}', 0, 1);
/*!40000 ALTER TABLE `favourite` ENABLE KEYS */;

-- Volcando estructura para tabla ci_crud.markers
CREATE TABLE IF NOT EXISTS `markers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `address` varchar(80) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `type` varchar(30) NOT NULL,
  `favourite_id` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ci_crud.markers: 8 rows
DELETE FROM `markers`;
/*!40000 ALTER TABLE `markers` DISABLE KEYS */;
INSERT INTO `markers` (`id`, `name`, `address`, `lat`, `lng`, `type`, `favourite_id`, `user_id`) VALUES
	(1, 'Love.Fish', '580 Darling Street, Rozelle, NSW', -33.861034, 151.171936, 'restaurant', 1, 1),
	(2, 'Young Henrys', '76 Wilford Street, Newtown, NSW', -33.898113, 151.174469, 'bar', 1, 1),
	(3, 'Hunter Gatherer', 'Greenwood Plaza, 36 Blue St, North Sydney NSW', -33.840282, 151.207474, 'bar', 1, 1),
	(4, 'The Potting Shed', '7A, 2 Huntley Street, Alexandria, NSW', -33.910751, 151.194168, 'bar', 1, 1),
	(5, 'Nomad', '16 Foster Street, Surry Hills, NSW', -33.879917, 151.210449, 'bar', 1, 1),
	(6, 'Three Blue Ducks', '43 Macpherson Street, Bronte, NSW', -33.906357, 151.263763, 'restaurant', 1, 1),
	(7, 'Single Origin Roasters', '60-64 Reservoir Street, Surry Hills, NSW', -33.881123, 151.209656, 'restaurant', 1, 1),
	(8, 'Red Lantern', '60 Riley Street, Darlinghurst, NSW', -33.874737, 151.215530, 'restaurant', 1, 1),
	(9, 'Trujillo/PE', 'Trujillo/PE', -8.110000, -79.029999, 'default', 26, 1),
	(10, 'Palermo/CO', 'Palermo/CO', 2.890000, -75.430000, 'default', 27, 1),
	(11, 'Colonia/RO', 'Colonia/RO', 46.599998, 23.969999, 'default', 28, 1),
	(12, 'Rome/IT', 'Rome/IT', 41.889999, 12.480000, 'default', 29, 1),
	(13, 'California/MX', 'California/MX', 24.049999, -104.610001, 'default', 30, 1),
	(14, 'Turin/IT', 'Turin/IT', 45.070000, 7.680000, 'default', 31, 1),
	(15, 'Naples/IT', 'Naples/IT', 40.840000, 14.250000, 'default', 32, 1),
	(16, 'Ross River/CA', 'Ross River/CA', 61.849998, -130.589996, 'default', 33, 1),
	(17, 'Dudinka/RU', 'Dudinka/RU', 69.339996, 84.570000, 'default', 34, 1),
	(18, 'Intenta acercando el mapa sobre una ciudad', 'Intenta acercando el mapa sobre una ciudad', 75.800003, -44.099998, 'default', 35, 1);
/*!40000 ALTER TABLE `markers` ENABLE KEYS */;

-- Volcando estructura para tabla ci_crud.session
CREATE TABLE IF NOT EXISTS `session` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ci_crud.session: ~6 rows (aproximadamente)
DELETE FROM `session`;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` (`id`, `ip_address`, `timestamp`, `data`) VALUES
	('2pmdtbk88j4lkvericsehvfsu2', '127.0.0.1', 1521237051, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373035313B),
	('33chtfcme7r05jperjlfmfccat', '127.0.0.1', 1521237063, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373036333B686F6C617C733A323A227475223B),
	('7o7v8kr81q4e9pj2ns41spms69', '127.0.0.1', 1521237060, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373036303B757365725F69647C693A313B757365726E616D657C733A393A22657269636B6F72736F223B6C6F676765645F696E7C623A313B69735F636F6E6669726D65647C623A303B69735F61646D696E7C623A303B),
	('h3t0ncdl9a5j91bgka95buothf', '127.0.0.1', 1521237055, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373035353B),
	('ik7u5qhecdpnntlcnsior7n78n', '::1', 1521237211, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373231313B686F6C617C733A323A227475223B),
	('l56bkn9dej7bsl0dr9t8p5ldt0', '::1', 1521237208, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373230373B757365725F69647C693A313B757365726E616D657C733A393A22657269636B6F72736F223B6C6F676765645F696E7C623A313B69735F636F6E6669726D65647C623A303B69735F61646D696E7C623A303B),
	('uj02jcb84dq22hp3f6s322m4gs', '::1', 1521237202, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313532313233373230323B);
/*!40000 ALTER TABLE `session` ENABLE KEYS */;

-- Volcando estructura para tabla ci_crud.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) DEFAULT 'default.jpg',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla ci_crud.users: ~4 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `email`, `password`, `avatar`, `created_at`, `updated_at`, `is_admin`, `is_confirmed`, `is_deleted`) VALUES
	(1, 'erickorso', 'erickorso@gmail.com', '$2y$10$NKDmDWYCHUy2NUVNDMqFSO1iTGpN.HwwbWSZbO5PMbAVoVcTFWROe', 'default.jpg', '2018-03-15 13:45:38', NULL, 0, 0, 0),
	(2, 'rodrigo', 'rodrigo@gmail.com', '$2y$10$DeA.hsxgtA8wNAxcAzINMOQe7YDhkc12FGcrkgGjgUA9JJdRO5Kz.', 'default.jpg', '2018-03-15 14:20:17', NULL, 0, 0, 0),
	(3, 'thaydee', 'thaydee@gmail.com', '$2y$10$8SrLSHIR3pWflyoik4vz9e2qtBpDBvbfx7Mi0z/RqBNqK4tMhAtMW', 'default.jpg', '2018-03-15 14:46:37', NULL, 0, 0, 0),
	(4, 'sofia', 'sofia@gmail.com', '$2y$10$l.5sMaQLMSFAh5rgsBQ7e.r4TdIPQXDhq8Ep6M8DBtK5fvOdjjc26', 'default.jpg', '2018-03-16 03:07:26', NULL, 0, 0, 0),
	(5, 'oriana', 'oriana@gmail.com', '$2y$10$jUe.bhXH39bNQl5.u8l5/u33MgGpTWBRnwtQhaWJRjujffdmrnfYa', 'default.jpg', '2018-03-17 03:01:36', NULL, 0, 0, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
